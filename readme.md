## Laravel 5.5 (LTS) API altyapısı

Kullanılan Kütüphaneler

* JWT-Auth - [tymondesigns/jwt-auth](https://github.com/tymondesigns/jwt-auth)
* Dingo API - [dingo/api](https://github.com/dingo/api)
* Laravel-CORS [barryvdh/laravel-cors](http://github.com/barryvdh/laravel-cors)
* Laravel Permissions [spatie/laravel-permission](http://github.com/spatie/laravel-permission)
* Activity Log [spatie/laravel-activitylog](http://github.com/spatie/laravel-activitylog)

Projede kullanılan her bir kütüphane aktif olarak kullandığım ve yıldız sayısı yüksek kütüphanelerdir. Her birinin topluluğu aktiftir.

Api Dökümantasyonu 

[Postman Dökümantasyonu](https://documenter.getpostman.com/view/58821/RWMLL6xd)

Dokümandaki http://laravel-api.local benim bu projeye özel domainimdir.

Kurulum

- composer install 
- php artisan migrate 