<?php

use Dingo\Api\Routing\Router;

$api = app(Router::class);

$api->version('v1', ['namespace' => 'App\Api\V1\Controllers'], function (Router $api) {
    $api->group(['prefix' => 'auth'], function (Router $api) {
        $api->post('signup', 'SignUpController@signUp');
        $api->post('login', 'LoginController@login');

        $api->post('recovery', 'ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'ResetPasswordController@resetPassword');

        $api->post('logout', 'LogoutController@logout');
        $api->post('refresh', 'RefreshController@refresh');
    });

    $api->group(['prefix' => 'role'], function (Router $api) {
        $api->resource('management', 'RoleController');
        $api->post('add-permission', 'RoleController@addPermissionToRole');
        $api->post('update-permission', 'RoleController@updatePermissionToRole');
        $api->post('revoke-permission', 'RoleController@rovekoPermissionToRole');
        $api->post('give-role-to-user', 'RoleController@giveRoleToUser');
        $api->post('remove-role-from-user', 'RoleController@removeRoleToUser');
        $api->get('users-by-role', 'RoleController@usersByRole');
    });

    $api->group(['prefix' => 'permission'], function (Router $api) {
        $api->resource('management', 'PermissionController');
        $api->get('users-by-permission', 'RoleController@usersByPermission');
    });

    $api->group(['middleware' => 'jwt.auth'], function (Router $api) {
        $api->get('me', 'UserController@me');
        $api->post('me/check-role', 'UserController@roleCheck');
        $api->post('me/check-permission', 'UserController@permissionCheck');
        $api->post('me/update', 'UserController@update');
        $api->post('me/update-password', 'UserController@updatePassword');
    });

    $api->get('info', function () {
        return response([
            'message' => 'Api V1'
        ]);
    });
});
