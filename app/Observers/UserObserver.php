<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 20.05.2018
 * Time: 22:59
 */

namespace App\Observers;

use App\Models\User;

class UserObserver
{
    /**
     * Listen to the User created event / Kullanıcı yaratıldığında çalışacak kısım
     * @param User $user
     */
    public function created(User $user)
    {
        //todo : creation log
    }

    /**
     * Listen to the User deleting event / Kullanıcı silinirken yaratılacak kısım
     *
     * @param User $user
     * @return void
     */
    public function deleting(User $user)
    {
        //todo : deletion log
    }
}