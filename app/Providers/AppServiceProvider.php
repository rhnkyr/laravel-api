<?php

namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        \Validator::extend('values', function ($attribute, $values, $parameters) {
            $value = explode(',', $values);
            if (\is_array($value)) {
                $rules = [
                    'value' => 'required|string|max:191',
                ];
                if ($value) {
                    foreach ($value as $email) {
                        $data      = [
                            'value' => $email
                        ];
                        $validator = \Validator::make($data, $rules);
                        if ($validator->fails()) {
                            return false;
                        }
                    }
                }
            }
            return true;
        });


        \Validator::replacer('values', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
