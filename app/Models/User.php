<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 28 Jul 2018 09:32:07 +0000.
 */

namespace App\Models;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, LogsActivity;

    protected static $logName = 'user-log';

    protected $guard_name = 'api';

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'remember_token'
    ];

    /**
     * Automatically creates hash for the user password. / Şifre hash
     *
     * @param  string $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT. / JWT token için belirleyici anahtar
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT. / Jwt için özel kelimler varsa burada belirliyoruz
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
