<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class PermissionToRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role'        => 'required|max:191',
            'permissions' => 'values' // We made a validator in App\Providers\AppServiceProvider.php
        ];
    }
}
