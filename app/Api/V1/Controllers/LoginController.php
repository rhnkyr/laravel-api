<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoginController extends Controller
{
    /**
     * Log the user in / Kullanıcı giriş
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = auth()->attempt($credentials);

            if (!$token) {
                throw new AccessDeniedHttpException();
            }

        } catch (\Exception $e) {
            throw new HttpException(500);
        }

        return response([
            'status'     => 'ok',
            'token'      => $token,
            'expires_in' => auth()->factory()->getTTL() * 60
        ], 200);
    }
}
