<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\GenericValueRequest;
use App\Api\V1\Requests\PermissionToRoleRequest;
use App\Api\V1\Requests\RemoveUserRoleRequest;
use App\Api\V1\Requests\RoleStoreRequest;
use App\Api\V1\Requests\RoleToUserRequest;
use App\Api\V1\Requests\RoleUpdateRequest;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource. / Rol listesini getirir
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return response($roles);
    }

    /**
     * Store a newly created resource in storage. / Yeni rol yaratır
     *
     * @param RoleStoreRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(RoleStoreRequest $request)
    {
        $role = Role::create($request->only('name', 'guard_name'));
        return response(compact('role'));
    }

    /**
     * Display the specified resource. / Verilen id ye göre rolü getiri
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $role = Role::findOrFail($id);
            return response(compact('role'));
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Update the specified resource in storage / Rol güncellemesi
     *
     * @param RoleUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $id)
    {
        try {
            $role = Role::findOrFail($id);

            $role->name = $request->name;
            if ($request->guard_name) {
                $role->guard_name = $request->guard_name;
            }
            $role->save();
            return response(compact('role'), 201);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Remove the specified resource from storage. / Rol silme
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $role = Role::findOrFail($id);
            $role->delete();
            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Add permission to specific role / İlgili role yetki ataması yapar
     * @param PermissionToRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function addPermissionToRole(PermissionToRoleRequest $request)
    {
        try {
            $value       = $request->role;
            $role        = Role::where('id', $value)->orWhere('name', $value)->firstOrFail();
            $permissions = explode(',', $request->permissions);
            $role->givePermissionTo($permissions);

            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Update permission to specific role / İlgili role yetki güncellemesi yapar
     * @param PermissionToRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function updatePermissionToRole(PermissionToRoleRequest $request)
    {
        try {
            $value       = $request->role;
            $role        = Role::where('id', $value)->orWhere('name', $value)->firstOrFail();
            $permissions = explode(',', $request->permissions);
            $role->syncPermissions($permissions);

            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Revoke permission to specific role / İlgili role yetki kaldırması yapar
     * @param PermissionToRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function rovekoPermissionToRole(PermissionToRoleRequest $request)
    {
        try {
            $value       = $request->role;
            $role        = Role::where('id', $value)->orWhere('name', $value)->firstOrFail();
            $permissions = explode(',', $request->permissions);
            $role->revokePermissionTo($permissions);

            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Give role to specific user / İlgili kullanıcıya rol ataması yapar
     * @param RoleToUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function giveRoleToUser(RoleToUserRequest $request)
    {
        try {
            $user  = User::findOrFail($request->user_id);
            $roles = explode(',', $request->roles);
            $user->assignRole($roles);
            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Remove role from specific user / İlgili kullanıcıya rol ataması yapar
     * @param RemoveUserRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function removeRoleToUser(RemoveUserRoleRequest $request)
    {
        try {
            $user = User::findOrFail($request->user_id);
            $user->removeRole($request->role);
            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * User list by role name / Rol adına göre kullanıcı listesi
     * @param GenericValueRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function usersByRole(GenericValueRequest $request)
    {
        $users = User::role($request->value)->get();
        return response($users);
    }

}
