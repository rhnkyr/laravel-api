<?php

namespace App\Api\V1\Controllers;

use Config;
use App\Models\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = new User($request->all());
        if (!$user->save()) {
            throw new HttpException(500);
        }

        //According to release token value we share or do not share the token / Token ı kayıttan sonra gösterip göstermeme durumu
        if (!Config::get('boilerplate.sign_up.release_token')) {
            return response([
                'status' => 'ok'
            ], 201);
        }

        //Retrieve the user's token after sing-up / Yeni yaratılan kullanıcının tokenı bulur
        $token = $JWTAuth->fromUser($user);

        return response([
            'status' => 'ok',
            'token'  => $token
        ], 201);
    }
}
