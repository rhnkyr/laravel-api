<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\GenericValueRequest;
use App\Api\V1\Requests\PermissionStoreRequest;
use App\Api\V1\Requests\PermissionUpdateRequest;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource. / Yetki listesini getirir
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        return response($permissions);
    }

    /**
     * Store a newly created resource in storage. / Yeni yetki yaratır
     *
     * @param PermissionStoreRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(PermissionStoreRequest $request)
    {
        $permission = Permission::create($request->only('name','guard_name'));
        return response(compact('permission'));
    }

    /**
     * Display the specified resource. / Verilen id ye göre yetkiyi getiri
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $permission = Permission::findOrFail($id);
            return response(compact('permission'));
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Update the specified resource in storage / Yetki güncellemesi
     *
     * @param PermissionUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionUpdateRequest $request, $id)
    {
        try {
            $permission = Permission::findOrFail($id);

            $permission->name = $request->name;
            if($request->guard_name) {
                $permission->guard_name = $request->guard_name;
            }
            $permission->save();
            return response(compact('permission'), 201);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Remove the specified resource from storage. / Yetki silme
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $permission = Permission::findOrFail($id);
            $permission->delete();
            return response(['status' => 'ok']);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * User list by permission name / Yetki adına göre kullanıcı listesi
     * @param GenericValueRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function usersByPermission(GenericValueRequest $request)
    {
        $users = User::permission($request->value)->get();
        return response($users);
    }
}
