<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

class RefreshController extends Controller
{
    /**
     * Refresh a token. / Token güncelleme
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $token = auth()->refresh();

        return response([
            'status'     => 'ok',
            'token'      => $token,
            'expires_in' => auth()->factory()->getTTL() * 60
        ],200);
    }
}
