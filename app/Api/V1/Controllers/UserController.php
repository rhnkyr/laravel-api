<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\PasswordUpdateRequest;
use App\Api\V1\Requests\UserPermissionRequest;
use App\Api\V1\Requests\UserRoleRequest;
use App\Api\V1\Requests\UserUpdateRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{

    /**
     * Get the authenticated User / Giriş yapan kullanıcının bilgisi
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        try {
            $me          = User::findOrFail(auth()->user()->id);
            $me['roles'] = $me->getPermissionsViaRoles();
            return response(['status' => 'ok', 'user' => $me]);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }
    }

    /**
     * Update user information / Kullanıcı bilgi güncelleme
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request)
    {
        $user       = auth()->user();
        $attributes = array_filter($request->only('name', 'email'));
        if ($attributes) {
            $user->update($attributes);
        }
        return response(['status' => 'ok'], 201);

    }

    /**
     * Edit user password / Kullanıcı şifre güncelleme
     * @param PasswordUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(PasswordUpdateRequest $request)
    {
        $attributes              = array_filter($request->only('password', 'new_password', 'new_password_confirmation'));
        $user                    = auth()->user();
        $credentials['email']    = $user->email;
        $credentials['password'] = $request->password;
        if (!$token = auth()->attempt($credentials)) {
            return response(['status' => 'nok', 'message' => 'Unauthorized.'], 401);
        }

        if ($attributes) {
            $password       = app('hash')->make($request->new_password);
            $user->password = $password;
            $user->save();
            return response(['status' => 'ok'], 201);
        }

        return response(['status' => 'nok', 'message' => 'Necessary attributes did not provided!']);
    }

    /**
     * Check use has specific role / Kullanıcı için belirli bir rolü kontrole eder
     * @param UserRoleRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function roleCheck(UserRoleRequest $request)
    {
        try {
            $user = User::findOrFail(auth()->user()->id);
            if (!$user->hasRole($request->role)) {
                return response(['status' => 'nok', 'message' => 'Unauthorized'], 401);
            }
            return response(['status' => 'ok'], 200);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }

    }

    /**
     * Check use has specific permission / Kullanıcı için belirli bir yetkiyi kontrole eder
     * @param UserPermissionRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function permissionCheck(UserPermissionRequest $request)
    {
        try {
            $user = User::findOrFail(auth()->user()->id);
            if (!$user->hasPermissionTo($request->permission)) {
                return response(['status' => 'nok', 'message' => 'Unauthorized'], 401);
            }
            return response(['status' => 'ok'], 200);
        } catch (ModelNotFoundException $exception) {
            return response(['status' => 'nok'], 404);
        }

    }
}
